import * as vscode from 'vscode';
import { createFakePartial } from '../../test_utils/create_fake_partial';
import { SupportedLanguagePolicy, UNSUPPORTED_LANGUAGE } from './supported_language_policy';
import { createActiveTextEditorChangeTrigger } from '../../test_utils/vscode_fakes';
import { AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES } from '../constants';

describe('SupportedLanguagePolicy', () => {
  let policy: SupportedLanguagePolicy;
  const triggerActiveTextEditorChange = createActiveTextEditorChangeTrigger();

  beforeEach(() => {
    policy = new SupportedLanguagePolicy();
  });

  it('has UNSUPPORTED_LANGUAGE state', () => {
    expect(policy.state).toBe(UNSUPPORTED_LANGUAGE);
  });

  it('is not engaged when language is supported', async () => {
    AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES.forEach(async languageId => {
      await triggerActiveTextEditorChange(
        createFakePartial<vscode.TextEditor>({ document: { languageId } }),
      );

      expect(policy.engaged).toBe(false);
    });
  });

  it('is engaged when language is NOT supported', async () => {
    ['html', 'markdown', 'css'].forEach(async languageId => {
      await triggerActiveTextEditorChange(
        createFakePartial<vscode.TextEditor>({ document: { languageId } }),
      );

      expect(policy.engaged).toBe(true);
    });
  });

  it('fires `onEngagedChange` event when the engaged changes', async () => {
    const listener = jest.fn();
    policy.onEngagedChange(listener);

    expect(listener).not.toHaveBeenCalled();

    await triggerActiveTextEditorChange(
      createFakePartial<vscode.TextEditor>({ document: { languageId: 'javascript' } }),
    );

    expect(listener).toHaveBeenCalledWith(false);

    await triggerActiveTextEditorChange(
      createFakePartial<vscode.TextEditor>({ document: { languageId: 'json' } }),
    );

    expect(listener).toHaveBeenCalledWith(true);
  });
});
