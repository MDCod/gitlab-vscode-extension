import * as vscode from 'vscode';
import { StatePolicy, VisibleState } from './state_policy';
import { diffEmitter } from '../diff_emitter';
import { AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES } from '../constants';

export const UNSUPPORTED_LANGUAGE: VisibleState = 'code-suggestions-document-unsupported-language';

export class SupportedLanguagePolicy implements StatePolicy {
  #subscriptions: vscode.Disposable[] = [];

  #eventEmitter = diffEmitter(new vscode.EventEmitter<boolean>());

  #isLanguageSupported = false;

  constructor() {
    this.#subscriptions.push(
      vscode.window.onDidChangeActiveTextEditor(async te => {
        this.#isLanguageSupported = te
          ? AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES.includes(te.document.languageId)
          : false;
        this.#eventEmitter.fire(this.engaged);
      }),
    );
  }

  async init() {
    await this.#checkLanguageSupported(vscode.window.activeTextEditor);
  }

  get engaged() {
    return !this.#isLanguageSupported;
  }

  state = UNSUPPORTED_LANGUAGE;

  onEngagedChange = this.#eventEmitter.event;

  dispose() {
    this.#subscriptions.forEach(s => s.dispose());
  }

  async #checkLanguageSupported(textEditor?: vscode.TextEditor) {
    this.#isLanguageSupported = textEditor
      ? AI_ASSISTED_CODE_SUGGESTIONS_LANGUAGES.includes(textEditor.document.languageId)
      : false;
    this.#eventEmitter.fire(this.engaged);
  }
}
