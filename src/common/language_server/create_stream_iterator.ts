import { Disposable } from 'vscode';
import { StreamingCompletionResponse, CancelStreaming } from '@gitlab-org/gitlab-lsp';
import { BaseLanguageClient, CancellationToken } from 'vscode-languageclient';
import { log } from '../log';

interface CompletionPart {
  completion: string;
  canceled?: boolean;
  done: boolean;
}

/*
 * Length of time to wait before timing-out a stream.
 *
 * Designates the maximum allowed amount of time in milliseconds from:
 * a notification arriving from the LS with a CompletionPart, to:
 * the client requesting that part from the queue.
 *
 * See https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1190 for more info
 */
export const STREAM_QUEUE_TIMEOUT_MS = 200;

export const createStreamIterator = (
  client: BaseLanguageClient,
  streamId: string,
  cancellationToken: CancellationToken,
  onCancelDetached: () => void,
): AsyncIterator<{ completion: string }, { completion: string; canceled?: boolean }> => {
  let resolveWait: ((val: CompletionPart) => void) | null = null;
  let lastCompletion = '';
  let detachedStreamCheck: ReturnType<typeof setTimeout> | undefined;

  const queue: CompletionPart[] = [];
  const subscriptions: Disposable[] = [];

  const onComplete = () => {
    subscriptions.forEach(x => x.dispose());
  };

  const resetCleanupCountdown = () => {
    if (detachedStreamCheck) {
      clearTimeout(detachedStreamCheck);
    }
    detachedStreamCheck = undefined;
  };

  const initiateCleanupCountdown = () => {
    if (detachedStreamCheck) {
      // Prevent concurrent countdowns
      return;
    }
    log.debug(`Possible detached stream, initiating check`);
    detachedStreamCheck = setTimeout(() => {
      // Will only run if not cancelled by resetCleanupCountdown
      log.debug(`Detached stream detected, performing cleanup`);

      // Cancel the Language Server stream
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      client.sendNotification(CancelStreaming, { id: streamId });

      // Unset the loading icon
      onCancelDetached();

      // Dispose of stream iterator listeners
      onComplete();
    }, STREAM_QUEUE_TIMEOUT_MS);
  };

  const waitForNext = (): Promise<CompletionPart> => {
    if (queue.length) {
      return Promise.resolve(queue.shift()!);
    }

    return new Promise<CompletionPart>(resolve => {
      resolveWait = resolve;
    });
  };

  const onNext = (part: CompletionPart): void => {
    lastCompletion = part.completion;

    if (resolveWait) {
      resolveWait(part);
      resolveWait = null;
    } else {
      queue.push(part);
      initiateCleanupCountdown();
    }
  };

  if (cancellationToken) {
    subscriptions.push(
      cancellationToken.onCancellationRequested(async () => {
        await client.sendNotification(CancelStreaming, { id: streamId });
        onNext({ completion: lastCompletion, done: true, canceled: true });
      }),
    );
  }

  subscriptions.push(
    client.onNotification(StreamingCompletionResponse, ({ id, completion, done }) => {
      if (id !== streamId) {
        return;
      }

      let completionValue = completion || '';

      if (!completionValue && done) {
        completionValue = lastCompletion;
      }

      onNext({
        completion: completionValue,
        done,
      });
    }),
  );

  return {
    async next() {
      resetCleanupCountdown();

      const { completion, done, canceled = false } = await waitForNext();

      if (canceled) {
        onComplete();
        return {
          value: {
            completion,
            canceled,
          },
          done: true,
        };
      }

      log.debug(`Streaming Suggestion: ${completion}, Done is ${done}`);

      if (done) {
        onComplete();
      }

      return {
        value: {
          completion,
          canceled,
        },
        done,
      };
    },
  };
};
